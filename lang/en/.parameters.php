<?
$MESS ['BACKLITCALENDAR_CALENDAR_FORM_NAME'] = "Form name";
$MESS ['BACKLITCALENDAR_CALENDAR_INPUT_NAME'] = "Field name";
$MESS ['BACKLITCALENDAR_CALENDAR_INPUT_VALUE'] = "Value of the first interval field";
$MESS ['BACKLITCALENDAR_CALENDAR_SHOW_TIME'] = "Enable time controls";
$MESS ['BACKLITCALENDAR_CALENDAR_HIDE_TIMEBAR'] = "Hide Time input field";
$MESS ['BACKLITCALENDAR_CALENDAR_HIGHLIGHT_DATES'] = "Dates for highlighting (comma separated in dd.mm.yyyy format)";
$MESS ['BACKLITCALENDAR_CALENDAR_HIGHLIGHT_COLOR'] = "Css color for highliting of date";
?>