<?
$MESS ['BACKLITCALENDAR_CALENDAR_FORM_NAME'] = "Имя формы";
$MESS ['BACKLITCALENDAR_CALENDAR_INPUT_NAME'] = "Имя первого поля интервала";
$MESS ['BACKLITCALENDAR_CALENDAR_INPUT_VALUE'] = "Значение первого поля интервала";
$MESS ['BACKLITCALENDAR_CALENDAR_SHOW_TIME'] = "Позволять вводить время";
$MESS ['BACKLITCALENDAR_CALENDAR_HIDE_TIMEBAR'] = "Скрывать поле для ввода времени";
$MESS ['BACKLITCALENDAR_CALENDAR_HIGHLIGHT_DATES'] = "Даты для подсветки (через запятую в формате dd.mm.yyyy)";
$MESS ['BACKLITCALENDAR_CALENDAR_HIGHLIGHT_COLOR'] = "Css цвет для подстветки даты";
?>