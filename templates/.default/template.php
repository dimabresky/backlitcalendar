<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$hash = md5(randString(20));
$icon_calendar_id = "backlitcalendar__icon-$hash";
$highlight_class = "backlitcalendar__highlight-color-$hash";
$input_name = htmlspecialcharsbx(CUtil::JSEscape($arParams['INPUT_NAME']));
$form_name = $arParams['FORM_NAME'] != '' ? htmlspecialcharsbx(CUtil::JSEscape($arParams['FORM_NAME'])) : '';
?>
<style>
    .<?= $highlight_class ?> {
        background-color: <?= ($arParams["HIGHLIGHT_COLOR"] ? htmlspecialchars($arParams["HIGHLIGHT_COLOR"]) : "lightgreen") ?>;
    }
</style>
<div class="backlitcalendar">
    <input name="<?= $arParams["INPUT_NAME"] ?>" type="text" value="<?= $arParams["INPUT_VALUE"] ?>" <?= (Array_Key_Exists("~INPUT_ADDITIONAL_ATTR", $arParams)) ? $arParams["~INPUT_ADDITIONAL_ATTR"] : "" ?>>
    <img id="<?= $icon_calendar_id ?>" src="/bitrix/js/main/core/images/calendar-icon.gif" alt="<?= GetMessage('calend_title') ?>" class="calendar-icon" onmouseover="BX.addClass(this, 'calendar-icon-hover');" onmouseout="BX.removeClass(this, 'calendar-icon-hover');" border="0"/>
</div>

<script>
    /**
     * @package backlitcalendar
     */
    BX.ready(function () {

        var calendar = new BX.JCCalendar();

        calendar._highlighting_dates = [];

        <?= json_encode($arResult["JS_TIMESTAMPS_HIGHLIGHT_DATES"]) ?>.forEach(function (date) {
            calendar._highlighting_dates.push(calendar._check_date(date).valueOf().toString());
        });


        calendar._source_set_layer = calendar._set_layer;

        calendar._set_layer = function () {

            var cells = [];

            this._source_set_layer();

            // highlighting dates
            if (this._current_layer) {

                cells = BX.findChildren(this._current_layer, {
                    tag: 'A',
                    className: 'bx-calendar-cell'
                }, true, true);

                if (BX.type.isArray(cells) && cells.length) {
                    cells.forEach(function (a) {

                        var mktime = a.dataset.date;

                        if (
                                !BX.hasClass(a, 'bx-calendar-date-hidden') &&
                                !BX.hasClass(a, 'bx-calendar-active') &&
                                !BX.hasClass(a, '<?= $highlight_class ?>') &&
                                calendar._highlighting_dates.indexOf(mktime) !== -1
                                ) {
                            BX.addClass(a, '<?= $highlight_class ?>');
                        }
                    });
                }


            }
        };
        
        document.getElementById("<?= $icon_calendar_id ?>").onclick = function () {

            calendar.Show({node: this, field: '<?= $input_name ?>', form: '<?= $form_name ?>', bTime: <?= $arParams['SHOW_TIME'] == 'Y' ? 'true' : 'false' ?>, currentTime: '<?= (time() + date("Z") + CTimeZone::GetOffset()) ?>', bHideTime: <?= $arParams['HIDE_TIMEBAR'] == 'Y' ? 'true' : 'false' ?>});

        };

    });
</script>