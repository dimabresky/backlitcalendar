<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/**
 * Bitrix календарь с подсветкой дат
 * 
 * @author ИП Бреский Дмитрий Игоревич <dimabresky@gmail.com>
 */
class BitrixBacklitCalendar extends CBitrixComponent {

    public function prepareParameters() {

        $this->arParams["HIGHLIGHT_DATES"] = \str_replace([" ", "\r\n"], ["", ""], $this->arParams["HIGHLIGHT_DATES"]);
    }

    public function executeComponent() {

        $this->prepareParameters();

        $this->arResult["JS_HIGHLIGHT_DATES"] = [];
        if (strlen($this->arParams["HIGHLIGHT_DATES"])) {
            $dates = explode(",", $this->arParams["HIGHLIGHT_DATES"]);
            foreach ($dates as $date) {
                
                if (\preg_match("#[0-9]{2}\.[0-9]{2}\.[0-9]{4}#", $date)) {
                    $this->arResult["JS_TIMESTAMPS_HIGHLIGHT_DATES"][] = $date;
                }
                
            }
        }
        
        \CJSCore::Init(array('popup', 'date'));
        $this->includeComponentTemplate();
    }

}
