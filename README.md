# backlitcalendar
Компонент используется для ввода даты/времени для BitrixFramework. В визуальном редакторе компонент расположен по пути: Служебные > Элемент управления Календарь с подсветкой дат.

Пример подключения на странице
```php
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?php 
$APPLICATION->IncludeComponent(
	"bitrix:backlitcalendar",
	"",
	Array(
		"FORM_NAME" => "",
		"HIDE_TIMEBAR" => "N",
		"HIGHLIGHT_COLOR" => "lightgreen",
		"HIGHLIGHT_DATES" => "15.05.2019, 21.05.2019",
		"INPUT_NAME" => "date_fld_1",
		"INPUT_VALUE" => "",
		"SHOW_TIME" => "N"
	)
);?>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
```

Демонстрация работы

![демонстрация работы](https://gitlab.com/dimabresky/backlitcalendar/raw/master/img.jpg)
