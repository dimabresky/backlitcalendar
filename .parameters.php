<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arComponentParameters = array(
    "PARAMETERS" => array(
        "FORM_NAME" => array(
            "NAME" => GetMessage("BACKLITCALENDAR_CALENDAR_FORM_NAME"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
            "PARENT" => "BASE",
        ),
        "INPUT_NAME" => array(
            "NAME" => GetMessage("BACKLITCALENDAR_CALENDAR_INPUT_NAME"),
            "TYPE" => "STRING",
            "DEFAULT" => "date_fld",
            "PARENT" => "BASE",
        ),
        "INPUT_VALUE" => array(
            "NAME" => GetMessage("BACKLITCALENDAR_CALENDAR_INPUT_VALUE"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
            "PARENT" => "BASE",
        ),
        "SHOW_TIME" => array(
            "NAME" => GetMessage("BACKLITCALENDAR_CALENDAR_SHOW_TIME"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "PARENT" => "BASE",
        ),
        "HIDE_TIMEBAR" => array(
            "NAME" => GetMessage("BACKLITCALENDAR_CALENDAR_HIDE_TIMEBAR"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "PARENT" => "BASE"
        ),
        "HIGHLIGHT_COLOR" => array(
            "NAME" => GetMessage("BACKLITCALENDAR_CALENDAR_HIGHLIGHT_COLOR"),
            "TYPE" => "STRING",
            "DEFAULT" => "lightgreen",
            "PARENT" => "BASE"
        ),
        "HIGHLIGHT_DATES" => array(
            "NAME" => GetMessage("BACKLITCALENDAR_CALENDAR_HIGHLIGHT_DATES"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
            "PARENT" => "BASE"
        ),
    ),
);
?>